# %% Funkce nacti
def nactiSlovnik(path_to_dic: str,
                 encoding='iso-8859-2',
                 validator: callable = lambda w: w
    ) -> list[str]:
    """Vytvoří seznam slov ze souboru.

    Args:
        path_to_dic (str): cesta k souboru slovniku
        validator: upravi slovo ve slovniku, nebo vraci None, když se má vyřadit
    Returns:
        list[str]: seznam slov
    """

# %% main block
if __name__ == '__main__': 
    slovnik = nactiSlovnik('static/cs_CZ.dic')
    print(slovnik[:10])
# %%
