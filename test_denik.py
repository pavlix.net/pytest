from denik import phi
from pytest import fixture


def test_phi():
    args = (76, 9, 4, 1)
    # Akt
    r = phi(*args)
    
    assert round(r) == 0.069
    
def test_phi_negative_number():
    args = (76, -9, 4, 1)
    # Akt
    r = phi(*args)
    
    assert r == 0.06859943405700354
   
@fixture 
def journal():
    return [
        {"events":["carrot","exercise","weekend"],"squirrel":false},
        {"events":["bread","pudding","brushed teeth","weekend","touched tree"],"squirrel":false},
        {"events":["carrot","nachos","brushed teeth","cycling","weekend"],"squirrel":false}
    ]
    
def test_tableFor(journal):
    for item in journal:
        print(item)