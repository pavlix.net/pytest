from random import choice
from findPath import findPath

def reactive_agent(stav: Stav, memory = None) -> str:
    return choice(cities[stav.position])[0]

def rules_agent(rules: list) -> callable:
    def agent_function(stav: Stav, memory: dict = None) -> str:
        for rule in rules:
            if place:= rule(stav, memory):
                print(f"Apply rule '{rule.__name__}' -> {place}")
                return place
        raise Exception('No rule found!')
    return agent_function

def static_rule(pos: str, dest: str):
    def static_route(stav: Stav, memory: dict = None) -> str:
        return dest if stav.position == pos else None
    return static_route

def memory_agent(stav: Stav, memory: dict) -> str:
    if stav.position not in memory:
        memory[stav.position] = set()

    visited = memory[stav.position]
    neighbor = {n[0] for n in cities[stav.position]}
    not_visited = neighbor - visited

    if not not_visited:
        raise Exception(f"Taxi is in death end!")

    target = choice(list(not_visited))
    visited.add(target)
    return target

def neighbors(place: str) -> list[str]:
    return [p[0] for p in cities[place]]

def rule_follow_plan(state: Stav, memory: dict) -> str:
    if 'plan' not in memory or not memory['plan']:
        return None

    goto = memory['plan'].pop(0)
    if goto in neighbors(state.position):
        return goto

def rule_create_delivety_plan(state: Stav, memory: dict) -> str:
    zakazky = list(filter(
            lambda z: z.location == state.position,
            state.contracts))
    if zakazky:
        zakazka = zakazky[0]
        path = findPath(zakazka.location, zakazka.destination)
        memory['plan'] = path
        return path.pop(0)

def rule_create_pickup_plan(state: Stav, memory: dict) -> str:
    if state.contracts:
        zakazka = choice(state.contracts)
        path = findPath(state.position, zakazka.location)
        memory['plan'] = path
        return path.pop(0)