def findPath(s: str, g: str, cities) -> list[str]:
    closed = {s}
    queue = [s]
    parrent = {s: None}
    while g not in parrent:
        if not queue: 
            raise Exception(f"Path form {s} to {g} not exists.") 
        u = queue.pop(0) 
        for s, d in cities[u]:
            if s not in closed:
                closed.add(s)
                queue.append(s)
                parrent[s] = u
    path = []
    while g:
        path.append(g)
        g = parrent[g]    
    return list(reversed(path))