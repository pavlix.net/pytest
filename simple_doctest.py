#!/usr/bin/env python

def sumStr(*args) -> str:
    """
    Concat all function agruments as Strngis to single output.
    
    For example:
    >>> sumStr(1, 'ABC', True, None, [])
    '1ABCTrueNone[]'
    
    
    """
    return "".join(map(str,args))

"""
Make sure our python interpretter is sane
>>> 2+5
7
"""
if __name__ == "__main__":
    import doctest
    doctest.testmod()
