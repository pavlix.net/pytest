# %% Nacteni journalu
import json

from symbol import eval_input

with open("journal.json") as journal_file:
    journal = json.load(journal_file)

len(journal)
# %%

print(journal[0])
# %% 
from collections import namedtuple

Table = namedtuple("Table", ["n00", "n01", "n10", "n11"])

a,b,c,d = Table(1,2,3,4)
a,b,c,d

# %% vypocet funkce podle https://eloquentjavascript.net/04_data.html

from math import sqrt

def phi(t: Table) -> float:
    """_summary_

    Args:
        n00 (_type_): _description_
        n01 (_type_): _description_
        n10 (_type_): _description_
        n11 (_type_): _description_
    """
    return ((t.n11*t.n00)-(t.n10*t.n01))/(sqrt(
        (t.n10+t.n11)*(t.n00+t.n01)*(t.n01+t.n11)*(t.n00+t.n10)))

pizza = Table(76, 9, 4, 1)
phi(pizza) # → 0.068599434


# %% Vypsani vsech exisujicích event, unikatni + abecedně setřizené

def events():
    """Vypiše seznam vsech ulaostí v dniku, abecetne setřizené, unikátní"""
    return len(journal)


events()


# %%
