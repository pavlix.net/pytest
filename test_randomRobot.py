import random
from robot.randomRobot import randomRobot
from robot.State import State

def test_randomRobot(city_ABC, monkeypatch):
    monkeypatch.setattr(random, 'choice', lambda l: 'B')
    stav = State('B', None)
    # Act
    dest = randomRobot(stav, city_ABC)
    # Assert
    assert dest == 'B'