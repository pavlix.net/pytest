PYTHON - TVORBA AUTOMATIZOVANÝCH TESTŮ
======================================

- **Lektor:** Ing. Jan kolomazník, Ph.D.
- **Mobil:** +420 732 568 669
- **Email:** jan.kolomaznik@gmail.com 

Programujete v Pythonu a rádi byste své schopnosti uplatnili při tvorbě automatizovaných testů? 
Obliba programovacího jazyka Python pro automatizaci a skriptování neustále roste. 
Softwarové firmy začínají Python vnímat jako univerzální jazyk pro tvorbu testů pro software napsaný v libovolném programovacím jazyce. 
Přihlaste se na praktický workshop, ve kterém si vyzkoušíte tvorbu automatizovaných testů pro software napsaný nejen v Pythonu, ale v libovolném programovacím jazyce. 
Vyzkoušejte si nástroje pro testování všeho možného od vašeho vlastního kódu přes API až po webové aplikace.


Náplň kurzu:
------------

- **[Základy testování](00-test.ipynb)**
    - Kvalita software
    - Granularita testů
    - Testování a automatizace
- **[Python jako testovací nástroj](01-test.python.ipynb)**
    - Používání assertů ve vlastním kódu
    - Doctesty
    - Typová kontrola
- **[Testovací framework (pytest)](02-test.pytest.ipynb)**
    - Kam psát testovacích funkce
    - Spouštění testů
    - Testování v Jupyter notebook
    - Hlavní výhody frameworku
- **[Testování kódu](03-test.code.ipynb)**
    - Jednotkové testy
    - Závislosti: `Fixture`
    - Parametrizované testy
    - Testování modulů, funkcí a tříd
- **[Psaní testovatelného kódu](04-test.principles.ipynb)**
    - Funkce bez vedlejších efektů
    - Objektově orientované programování
    - OOP best practices
- **[Negativní testování](05-test.exceptions.ipynb)**
    - Očekávání výjimek
    - Očekávání že test spadne `@pytest.mark.xfail`
- **[Test-driven development](06-test.test_driven_development.ipynb)**
    - Vývoj software s použitím testů
- **[Testování celých aplikaci](07-test.application.ipynb)**
- **[Testování databázových aplikací](08-test.db.ipynb)**
    - Fixtures
- **[Testování aplikací přes API](09-test.api.ipynb)**
    - Komunikace a vzdálené volání
    - Práce s neznámým stavem
    - Restování selhání http komunikace
- **[estování webových a GUI aplikací](10-test.ui.ipynb)**
    - Testování webových dotazů
    - Možnosti testování GUI
- **[Seznam pytest pluginu pro lepší svět](11-test.pytest.plugins.ipynb)**
    - Výstupy testů a reporty
    - Změna chování pytest
    - Performance a čas
    - Práce s fixtures
    - Práce se sítí
