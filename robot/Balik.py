from collections import namedtuple

Balik = namedtuple('Balik', ['misto', 'adresa'])

from random import choices

def iRandomBaliky(places):
    history = set()
    while True:
        
        pocet_pokusu = 3
        mista = tuple(choices(places, k=2))
        
        while mista[0] == mista[1] or mista in history:
            mista = tuple(choices(places, k=2))
            
            pocet_pokusu -= 1
            if not pocet_pokusu:
                return
            
        history.add(mista)
        yield Balik(mista[0], mista[1])