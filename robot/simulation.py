from State import State
from itertools import count

def simulation(stav: State, fRobot, city):
    c = count(1)
    while stav.baliky:
        kam = fRobot(stav, city)
        stav = stav.presunRobota(kam)
        print(f"{next(c)}: kam {kam}, baliku {len(stav.baliky)}")
