def loadCity(roads: str) -> dict:
    city = {} #{ nazev:list[mista]}
    for a,b in map(lambda r: r.split('-'), roads):
        #a,b = road.split('-')
        city[a] = city[a] | {b} if a in city else {b}
        city[b] = city[b] | {a} if b in city else {a}
    return city