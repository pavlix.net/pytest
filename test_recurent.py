import pytest

def f():
    f()
        
def test_recursion_depth():
    with pytest.raises(RuntimeError) as excinfo:
        f()
        
    assert "maximum recursion" in str(excinfo.value)