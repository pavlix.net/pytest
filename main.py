from fastapi import FastAPI
from itertools import count

app = FastAPI()
counter = count()

@app.get("/{name}")
async def root(name: str = "world"):
    return {
        "message": f"Hello {name}!",
        "counter": next(counter)    
    }